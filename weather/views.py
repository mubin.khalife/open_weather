import requests
from django.shortcuts import render
from django.contrib import messages
import os


def index(request):
    url = "http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&APPID=" + \
        os.getenv("OPEN_WEATHER_API_ID")

    city_weather = {}

    if request.method == "POST":
        city = (request.POST.get("my_city"))
        r = requests.get(url.format(city)).json()
        try:
            city_weather = {
                'city': city,
                'temperature': r['main']['temp'],
                'description': r['weather'][0]['description'],
                'icon': r['weather'][0]['icon'],
            }
        except KeyError:
            messages.error(request, ("Data for entered city not found"))

    context = {'city_weather': city_weather}
    return render(request, "weather/weather.html", context)
