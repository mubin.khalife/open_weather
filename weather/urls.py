from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name="home"),
    # path('delete/<id>', views.delete, name="delete"),
    # re_path('^delete/(?P<id>\d)/$', views.delete, name="delete"),
]
